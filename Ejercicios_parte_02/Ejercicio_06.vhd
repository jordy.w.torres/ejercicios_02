
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Ejercicio_06 is
    Port ( clk, RST : in  STD_LOGIC;
           Q : out  STD_LOGIC_VECTOR (2 downto 0));
end Ejercicio_06;

architecture Arq_06 of Ejercicio_06 is
signal Qaux : STD_LOGIC_VECTOR (2 downto 0);
begin
	Q <= Qaux;
	process(clk, RST)
	begin
		if ( clk'event and clk='1') then
			if(RST = '0' or Qaux ="101") then
				Qaux <= "000";
			elsif(Qaux = "000") then
				Qaux <= Qaux+1;
			else
				Qaux <= Qaux+2;
			end if;
		end if;
	end process;
end Arq_06;
