-- Jainer Pinta
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Ejercicio_02 is
    Port ( T, clk : in  STD_LOGIC;
           Q, Qn : out  STD_LOGIC);
end Ejercicio_02;

architecture arqFFT of Ejercicio_02 is
signal Qaux: STD_LOGIC := '0';
begin
	Q <= Qaux;
	Qn <= not Qaux;
	process(T, clk)
	begin
		if (clk'event and clk = '1') then
			if (T='1') then
				Qaux <= not Qaux;
			end if;
		end if;
	end process;
end arqFFT;
