----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:03:16 07/14/2022 
-- Design Name: 
-- Module Name:    Ejercicio_01 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Ejercicio_01 is
     port (J,K,clk :in STD_LOGIC;
	         Q,M: out STD_LOGIC);
end Ejercicio_01;

architecture Behavioral of Ejercicio_01 is
signal aux :std_logic;
begin
     Q <= aux;
	  M <=not aux;
	  process(J,K,clk)
     begin 
	  if(clk'event and clk='1') then
	  if(J='0' and K ='0')then 
	      aux <= aux;
	   elsif (J='0' and K='1') then 
		aux <= '0';
	    elsif(J='1' and K='0') then
		 aux <='1';
		 else
		 aux <= not aux;
		 end if;
		 end if;
		 end process;
end Behavioral;

